package edu.ucsd.cse110.library;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import edu.ucsd.cse110.library.rules.RuleObjectBasedLateFeesStrategy;
import edu.ucsd.cse110.library.*;

public class TestPublication {
	
	Publication pub;
	Member max;
	Library lib;
	
	@Before
	public void setUp() {
		pub = new Book("Lords of the Rings", new RuleObjectBasedLateFeesStrategy());
		max = new Member("Max", MemberType.Teacher);
		lib = new Library();
	}
	
	/*
	@Test
	public void testCheckout() throws ParseException {
		LocalDate checkoutDate = LocalDate.of(20014, 12, 1);
		pub.checkout(max,checkoutDate);
		assertTrue(pub.isCheckout());
		assertEquals(max, pub.getMember());
		assertEquals(checkoutDate, pub.getCheckoutDate());
	}
	
	@Test
	public void testReturnOk() throws ParseException {
		LocalDate checkoutDate = LocalDate.of(20014, 12, 1);
		LocalDate returnDate  = LocalDate.of(20014, 12, 3);
		pub.checkout(max,checkoutDate);
		pub.pubReturn(returnDate);
		assertFalse(pub.isCheckout());
		assertEquals(0.00, max.getDueFees(),0.01);
	}
	
	@Test
	public void testReturnLate() throws ParseException {
		LocalDate checkoutDate = LocalDate.of(20014, 12, 1);
		LocalDate returnDate  = LocalDate.of(20014, 12, 20);
		pub.checkout(max,checkoutDate);
		pub.pubReturn(returnDate);
		assertFalse(pub.isCheckout());
		assertEquals(5.00, max.getDueFees(),0.01);
	}
	*/
	
	@Test
	public void testCheckout() throws ParseException {
		LocalDate checkoutDate = LocalDate.of(20014, 12, 1);
		lib.checkoutPublication(max,checkoutDate,pub);
		assertTrue(pub.isCheckout());
		assertEquals(max, pub.getMember());
		assertEquals(checkoutDate, pub.getCheckoutDate());
	}
	
	@Test
	public void testReturnOk() throws ParseException {
		LocalDate checkoutDate = LocalDate.of(20014, 12, 1);
		LocalDate returnDate  = LocalDate.of(20014, 12, 3);
		lib.checkoutPublication(max,checkoutDate,pub);
		lib.returnPublication(returnDate,pub);
		assertFalse(pub.isCheckout());
		assertEquals(0.00,lib.getFee(max),0.01);
	}
	
	@Test
	public void testReturnLate() throws ParseException {
		LocalDate checkoutDate = LocalDate.of(20014, 12, 1);
		LocalDate returnDate  = LocalDate.of(20014, 12, 20);
		lib.checkoutPublication(max,checkoutDate,pub);
		lib.returnPublication(returnDate,pub);
		assertFalse(pub.isCheckout());
		assertEquals(5.00,lib.getFee(max),0.01);
	}
	
}
