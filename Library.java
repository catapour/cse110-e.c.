package edu.ucsd.cse110.library;

import java.time.LocalDate;
//import java.util.ArrayList;

public class Library {
	
	void checkoutPublication(Member member, LocalDate checkoutDate, Publication pub)
	{
	  	pub.checkout(member, checkoutDate);
	} // checkoutPublication()
	
	void returnPublication(LocalDate returndate, Publication pub)
	{
	    pub.pubReturn(returndate);
	} // returnPublication()
	
	double getFee(Member member)
	{
		return member.getDueFees();
	} // getFee()
	
	boolean hasFee(Member member)
	{
		return (member.getDueFees() != 0);
	} // hasFee()

}
